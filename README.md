# Hooks
WordPress-like filter and action hooks implementation.

## Installation
```bash
npm install @peepso/hooks --save
```

## Usage
```javascript
import Hooks from '@peepso/hooks';

// You can instantiate it manually like in the example below:
let hooks = new Hooks();
hooks.addAction( 'actionName', 'fnName', ( ...args ) => console.log( args ) );

// Or, you can extend it to another class:
class Person extends Hooks {
	constructor() {
		this.addAction( 'actionName', 'fnName', ( ...args ) => console.log( args ) );
	}
}
```

## Methods

### Filter methods
`addFilter( filterName, handlerName, handler, [priority=10] )`  
`removeFilters( filterName, [handlerName, [handler]] )`  
`applyFilters( filterName, ...args )`  

### Action methods
`addAction( actionName, handlerName, handler, [priority=10] )`  
`removeActions( actionName, [handlerName, [handler]] )`  
`doAction( actionName, ...args )`  
