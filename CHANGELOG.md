## [1.0.3](https://gitlab.com/rsusanto/peepso-package-hooks/compare/v1.0.2...v1.0.3) (2019-04-26)


### Bug Fixes

* **changelog:** commit changelog after release. ([b684bc8](https://gitlab.com/rsusanto/peepso-package-hooks/commit/b684bc8))
