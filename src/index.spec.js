/* global describe test expect beforeEach afterEach */
import Hooks from '../dist';

let hooks;

beforeEach( () => ( hooks = new Hooks() ) );
afterEach( () => ( hooks = undefined ) );

describe( 'filter hook implementation', () => {
	test( 'should be able to add a filter hook handler', () => {
		expect( hooks.addFilter( 'filterName', 'fnName', () => {} ) ).toBe( true );
	} );

	test( 'should be able to remove all filter hook handlers', () => {
		hooks.addFilter( 'filterName', 'fnName1', () => {} );
		hooks.addFilter( 'filterName', 'fnName1', () => {} );
		hooks.addFilter( 'filterName', 'fnName2', () => {} );
		expect( hooks.removeFilters( 'filterName' ) ).toBe( 3 );
	} );

	test( 'should be able to remove all filter hook handlers with the same handler name', () => {
		hooks.addFilter( 'filterName', 'fnName1', () => {} );
		hooks.addFilter( 'filterName', 'fnName1', () => {} );
		hooks.addFilter( 'filterName', 'fnName2', () => {} );
		expect( hooks.removeFilters( 'filterName', 'fnName1' ) ).toBe( 2 );
	} );

	test( 'should be able to remove a specific filter hook handler', () => {
		let fn = () => {};
		hooks.addFilter( 'filterName', 'fnName1', fn );
		hooks.addFilter( 'filterName', 'fnName1', () => {} );
		hooks.addFilter( 'filterName', 'fnName2', () => {} );
		expect( hooks.removeFilters( 'filterName', 'fnName1', fn ) ).toBe( 1 );
	} );

	test( 'should execute all filter hook handlers in the correct order', () => {
		hooks.addFilter( 'filterName', 'fnName1', ( data, ...args ) => data + args[ 0 ], 9 );
		hooks.addFilter( 'filterName', 'fnName2', ( data, ...args ) => data + args[ 1 ], 11 );
		hooks.addFilter( 'filterName', 'fnName3', ( data, ...args ) => data + args[ 2 ] );
		expect( hooks.applyFilters( 'filterName', '', 'ab', 'cd', 'ef' ) ).toBe( 'abefcd' );
	} );

	test( 'should return the same data if no filter hook handlers are registered', () => {
		expect( hooks.applyFilters( 'filterName', 'ab' ) ).toBe( 'ab' );
	} );
} );

describe( 'action hook implementation', () => {
	test( 'should be able to add an action hook handler', () => {
		expect( hooks.addAction( 'actionName', 'fnName', () => {} ) ).toBe( true );
	} );

	test( 'should be able to remove all action hook handlers', () => {
		hooks.addAction( 'actionName', 'fnName1', () => {} );
		hooks.addAction( 'actionName', 'fnName1', () => {} );
		hooks.addAction( 'actionName', 'fnName2', () => {} );
		expect( hooks.removeActions( 'actionName' ) ).toBe( 3 );
	} );

	test( 'should be able to remove all action hook handlers with the same handler name', () => {
		hooks.addAction( 'actionName', 'fnName1', () => {} );
		hooks.addAction( 'actionName', 'fnName1', () => {} );
		hooks.addAction( 'actionName', 'fnName2', () => {} );
		expect( hooks.removeActions( 'actionName', 'fnName1' ) ).toBe( 2 );
	} );

	test( 'should be able to remove a specific action hook handler', () => {
		let fn = () => {};
		hooks.addAction( 'actionName', 'fnName1', fn );
		hooks.addAction( 'actionName', 'fnName1', () => {} );
		hooks.addAction( 'actionName', 'fnName2', () => {} );
		expect( hooks.removeActions( 'actionName', 'fnName1', fn ) ).toBe( 1 );
	} );

	test( 'should execute all action hook handlers in the correct order', () => {
		let data = '';
		hooks.addAction( 'actionName', 'fnName1', ( ...args ) => ( data += args[ 0 ] ), 9 );
		hooks.addAction( 'actionName', 'fnName2', ( ...args ) => ( data += args[ 1 ] ), 11 );
		hooks.addAction( 'actionName', 'fnName3', ( ...args ) => ( data += args[ 2 ] ) );
		hooks.doAction( 'actionName', 'ab', 'cd', 'ef' );
		expect( data ).toBe( 'abefcd' );
	} );

	test( 'should not modify any passed parameters', () => {
		let data = '';
		hooks.addAction( 'actionName', 'fnName1', () => 'gh' );
		hooks.addAction( 'actionName', 'fnName2', ( ...args ) => ( data += args.join( '' ) ) );
		hooks.doAction( 'actionName', 'ab', 'cd', 'ef' );
		expect( data ).toBe( 'abcdef' );
	} );
} );
