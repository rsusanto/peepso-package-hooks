/**
 * WordPress-like filter and action hooks implementation.
 */
class Hooks {
	constructor() {
		/**
		 * Hook handlers table.
		 *
		 * @private
		 * @type {Object.<string, Array>}
		 */
		this._hooks = {};
	}

	/**
	 * Register a hook handler.
	 *
	 * @private
	 * @param {string} hookName
	 * @param {string} fnName
	 * @param {Function} fn
	 * @param {number} [priority=10]
	 * @return {boolean}
	 */
	_addHook( hookName, fnName, fn, priority = 10 ) {
		let handlers = this._hooks[ hookName ],
			handler = { fnName, fn, priority };

		// Initialize and add the first handler directly if the handler list is empty.
		if ( ! handlers ) {
			this._hooks[ hookName ] = [ handler ];
			return true;
		}

		// Find the correct index to put the handler in the handler list.
		let index = handlers.length;
		for ( ; index > 0; index-- ) {
			if ( priority >= handlers[ index - 1 ].priority ) break;
		}

		// Insert handler in the handler list.
		handlers.splice( index, 0, handler );

		return true;
	}

	/**
	 * Remove specific hook handlers.
	 *
	 * @private
	 * @param {string} hookName
	 * @param {string} [fnName] - If specified, only remove hook handlers with the same name.
	 * @param {Function} [fn] - If specified, only remove hook handlers registered with the exact handler.
	 * @returns {number} The number of removed handlers.
	 */
	_removeHooks( hookName, fnName, fn ) {
		let handlers = this._hooks[ hookName ],
			counter = 0;

		// Remove all handlers if the second argument is not provided.
		if ( ! fnName ) {
			if ( handlers ) {
				counter = handlers.length;
				delete this._hooks[ hookName ];
			}
			return counter;
		}

		// Iterate through the handler list and remove all matching handlers.
		for ( let i = handlers.length - 1; i >= 0; i-- ) {
			if ( handlers[ i ].fnName === fnName ) {
				if ( ! fn || fn === handlers[ i ].fn ) {
					handlers.splice( i, 1 );
					counter++;
				}
			}
		}

		// Delete empty handler list.
		if ( ! handlers.length ) {
			delete this._hooks[ hookName ];
		}

		return counter;
	}

	/**
	 * Execute all hook handlers.
	 *
	 * @private
	 * @param {string} hookName
	 * @param {boolean} returnFirstArg
	 * @param {...*} [args]
	 * @returns {*}
	 */
	_executeHooks( hookName, returnFirstArg, ...args ) {
		let handlers = this._hooks[ hookName ],
			firstArg;

		if ( returnFirstArg ) {
			firstArg = args[ 0 ];
		}

		if ( handlers ) {
			for ( let i = 0, result; i < handlers.length; i++ ) {
				result = handlers[ i ].fn.apply( null, args );
				if ( returnFirstArg ) {
					args[ 0 ] = firstArg = result;
				}
			}
		}

		return firstArg;
	}

	/**
	 * Register a filter hook handler.
	 *
	 * @param {string} filterName
	 * @param {string} fnName
	 * @param {Function} fn
	 * @param {number} [priority=10]
	 * @return {boolean}
	 */
	addFilter( filterName, ...args ) {
		return this._addHook( `filter/${ filterName }`, ...args );
	}

	/**
	 * Remove specific filter hook handlers.
	 *
	 * @param {string} filterName
	 * @param {string} [fnName] - If specified, only remove hook handlers with the same name.
	 * @param {Function} [fn] - If specified, only remove hook handlers registered with the exact handler.
	 * @returns {number} The number of removed handlers.
	 */
	removeFilters( filterName, ...args ) {
		return this._removeHooks( `filter/${ filterName }`, ...args );
	}

	/**
	 * Execute all filter hook handlers.
	 *
	 * @param {string} filterName
	 * @param {...*} [args]
	 * @returns {*}
	 */
	applyFilters( filterName, ...args ) {
		return this._executeHooks( `filter/${ filterName }`, true, ...args );
	}

	/**
	 * Register an action hook handler.
	 *
	 * @param {string} actionName
	 * @param {string} fnName
	 * @param {Function} fn
	 * @param {number} [priority=10]
	 * @return {boolean}
	 */
	addAction( actionName, ...args ) {
		return this._addHook( `action/${ actionName }`, ...args );
	}

	/**
	 * Remove specific action hook handlers.
	 *
	 * @param {string} actionName
	 * @param {string} [fnName] - If specified, only remove hook handlers with the same name.
	 * @param {Function} [fn] - If specified, only remove hook handlers registered with the exact handler.
	 * @returns {number} The number of removed handlers.
	 */
	removeActions( actionName, ...args ) {
		return this._removeHooks( `action/${ actionName }`, ...args );
	}

	/**
	 * Execute all action hook handlers.
	 *
	 * @param {string} actionName
	 * @param {...*} [args]
	 */
	doAction( actionName, ...args ) {
		this._executeHooks( `action/${ actionName }`, false, ...args );
	}
}

export default Hooks;
